import {CanDeactivate} from '@angular/router';
import {Observable, of} from 'rxjs/index';
import {ContactsComponent} from '../../app/contacts/contacts.component';

export class ContactsGuard implements CanDeactivate<ContactsComponent> {
  public canDeactivate(component: ContactsComponent): Observable<boolean> {
    console.log(component.getOpenedTab());
    return of(true);
  }
}
