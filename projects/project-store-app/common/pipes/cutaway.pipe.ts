import {Pipe, PipeTransform} from '@angular/core';
import {UserModel} from '../models/user.model';

@Pipe({name: 'cutaway'})

export class CutawayPipe implements PipeTransform {
  public transform(userModel: UserModel, value: string) {
    switch (value) {
      case 'name':
        return userModel.name;
      case 'email':
        return userModel.email;
      default:
        return 'Bad data';
    }
  }
}
