import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'transformToArray'})

export class TransformMapToArrayPipe implements PipeTransform {
  public transform(maps: Map<string, any[]>): any[] {
    const array: any[] = [];
    if (maps) {
      maps.forEach((map) => {
        array.push(map);
      });
    }
    return array;
  }
}
