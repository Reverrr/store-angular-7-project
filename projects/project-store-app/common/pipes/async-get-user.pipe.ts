import {Pipe, PipeTransform} from '@angular/core';
import {UserService} from '../services/user.service';
import {UserModel} from '../models/user.model';

@Pipe({
  name: 'asyncGetUser',
  pure: false
})

export class AsyncGetUserPipe implements PipeTransform {
  private cachedUser: UserModel = {} as UserModel;
  private userId: number;
  constructor(private userService: UserService) {}
  public transform(id: number) {
    if (this.userId !== id) {
      this.userId = id;
      this.userService.getUser(id).subscribe((userValue) => {
        this.cachedUser = userValue;
      });
    }
    return this.cachedUser['name'];
  }
}
