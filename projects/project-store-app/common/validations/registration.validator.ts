import {ValidationErrors} from '@angular/forms';

export class RegistrationValidator {
  public static emailValidator(item): null | ValidationErrors {
    const reg = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (reg.test(item.value)) {
      return null;
    }
    return {email: true};
  }
}
