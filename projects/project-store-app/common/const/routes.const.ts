import {Routes} from '@angular/router';
import {BaseComponent} from '../../app/base/base.component';
import {MainPageComponent} from '../../app/main-page/main-page.component';
import {AboutUsComponent} from '../../app/about-us/about-us.component';
import {ContactsComponent} from '../../app/contacts/contacts.component';
import {NewsComponent} from '../../app/news/news.component';
import {NotFoundComponent} from '../../app/not-found/not-found.component';
import {AllUsersComponent} from '../../app/users/all-users/all-users.component';
import {UserComponent} from '../../app/users/user/user.component';
import {LoginComponent} from '../../app/login/login.component';
import {RegistrationComponent} from '../../app/registration/registration.component';
import {ContactsGuard} from '../guards/contacts.guard';
import {BaseResolver} from '../resolvers/base.resolver';
import {StoreComponent} from '../../app/store/store.component';
import {DeliveryComponent} from '../../app/delivery/delivery.component';
import {DeliveryResolver} from '../resolvers/delivery.resolver';

export const ROUTES: Routes = [
  {
    path: '',
    component: BaseComponent,
    children: [
        {
        path: '',
        component: MainPageComponent,
        pathMatch: 'full',
      }, {
        path: 'about-us',
        component: AboutUsComponent
      }, {
        path: 'contacts',
        component: ContactsComponent,
        canDeactivate: [ContactsGuard]
      }, {
        path: 'news',
        component: NewsComponent
      }, {
        path: 'delivery',
        component: DeliveryComponent,
        resolve: {
          delivery: DeliveryResolver
        }
      }, {
        path: 'store',
        component: StoreComponent,
      }, {
        path: 'users',
        component: AllUsersComponent,
      }, {
        path: 'users/:userId',
        component: UserComponent,
        resolve: {
          users: BaseResolver
        }
      }, {
      path: 'authorization/login',
      component: LoginComponent
      }, {
      path: 'authorization/registration',
      component: RegistrationComponent
      }, {
      path: 'lazy-loaded-module',
      loadChildren: './lazy-loaded/lazy-loaded.module#LazyLoadedModule'
      }
    ]
  },
  {  path: '**', component: NotFoundComponent }
];
