import {SelectOptionDateInterface} from '../interface/select-option-date.interface';

export class SelectOptionDateModel implements SelectOptionDateInterface {
  public key: string;
  public value: string;
  constructor (option: SelectOptionDateInterface = {} as SelectOptionDateInterface) {
    this.key = option.key;
    this.value = option.value;
  }
}
