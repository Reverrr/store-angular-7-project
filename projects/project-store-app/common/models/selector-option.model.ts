import {SelectorOptionInterface} from '../interface/selector-option.interface';

export class SelectorOptionModel<T> implements SelectorOptionInterface<T> {
  public label: string;
  public value: T;
  public checked: boolean;
  constructor(params: SelectorOptionInterface<T> = {} as SelectorOptionInterface<T>) {
    this.label = params.label;
    this.value = params.value;
    this.checked = params.checked;
  }
}

