import {AppStateInterface} from '../interface/app-state.interface';

export class AppStateModel implements AppStateInterface {
  public url: string;
  public component: string;
  constructor(params: AppStateInterface = {} as AppStateInterface) {
    this.url = params.url;
    this.component = params.component;
  }
}
