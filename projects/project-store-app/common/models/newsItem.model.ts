export class NewsItemModel {
  public id: string;
  public title: string;
  public text: string;
  constructor(options) {
    this.id = options.id;
    this.title = options.title;
    this.text = options.text;
  }
}
