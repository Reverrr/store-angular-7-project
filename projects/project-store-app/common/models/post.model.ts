import {PostInterface} from '../interface/post.interface';

export class PostModel implements PostInterface {
  public userId: string;
  public id: string;
  public title: string;
  public body: string;
  constructor(options: PostInterface = {} as PostInterface ) {
    this.userId = options.userId;
    this.id = options.id;
    this.title = options.title;
    this.body = options.body;
  }
}
