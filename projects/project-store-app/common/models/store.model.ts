import {StoreInterface} from '../interface/store.interface';

export class StoreModel implements StoreInterface {
  public id: string;
  public title: string;
  public price: string;
  public url: string;
  public count: number;

  constructor (params: StoreInterface = {} as StoreInterface) {
    this.id = params.id;
    this.title = params.title;
    this.price = params.price;
    this.url = params.url;
    this.count = params.count;
  }
}
