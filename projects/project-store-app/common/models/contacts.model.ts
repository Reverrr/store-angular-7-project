import {ContactsInterface} from '../interface/contacts.interface';

export class ContactsModel<T> implements ContactsInterface<T> {
  public id: string;
  public value: Array<T>;
  public isOpen?: boolean = false;
  constructor(options) {
    this.id = options.id;
    this.value = options.value;
    this.isOpen = options.isOpen;
  }
}
