import {UserInterface} from '../interface/user.interface';

export class UserModel implements UserInterface {
  public id: string;
  public name: string;
  public username: string;
  public email: string;
  public address: string;

  constructor(params: UserInterface = {} as UserInterface) {
    this.id = params.id;
    this.name = params.name;
    this.username = params.username;
    this.email = params.email;
    this.address = params.address;
  }
}
