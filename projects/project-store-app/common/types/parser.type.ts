export type ParserType<T, R> = (data: T) => R;
