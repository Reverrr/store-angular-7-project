
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {forkJoin, Observable, Observer, of} from 'rxjs/index';
import {PostModel} from '../models/post.model';
import {Injectable} from '@angular/core';
import {PostsHelper} from '../helpers/posts.helper';
import {KeyValueInterface} from '../interface/key-value.interface';
import {map} from 'rxjs/internal/operators';

@Injectable({providedIn: 'root'})

export class PostService {
  constructor(private http: HttpClient) {}

  public getPosts(url: string): Observable<PostModel[]> {
    return Observable.create((observer: Observer<PostModel[]>): void => {
      this.http.get<KeyValueInterface<any>[]>(url).subscribe(
        (posts: KeyValueInterface<any>[]): void => {
          observer.next(posts.map<PostModel>(PostsHelper.createPostModel));
          observer.complete();
        },
      (error: HttpErrorResponse) => observer.error(error),
      () => {}
      );
    });
  }

  public getPost(postId: string): Observable<PostModel> {
    return Observable.create((observer: Observer<PostModel>) => {
      this.http.get<PostModel>(`https://jsonplaceholder.typicode.com/posts/${postId}`).subscribe(
        (data: PostModel) => {
          observer.next(PostsHelper.createPostModel(data));
          observer.complete();
        },
        (error: HttpErrorResponse) => observer.error(error),
        () => {},
      );
    });
  }
  public getPostUser(postId: string) {
      return forkJoin<PostModel, string>([this.http.get<PostModel>(`https://jsonplaceholder.typicode.com/posts/${postId}`).pipe(map(
        (data: PostModel) => {
          return PostsHelper.createPostModel(data);
        }
      )), of('asdasd')]);
  }
}
