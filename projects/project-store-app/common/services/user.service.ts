import { Injectable } from '@angular/core';
import { UserHelper } from '../helpers/user.helper';
import { UserModel } from '../models/user.model';
import {BehaviorSubject, Observable} from 'rxjs/index';
import { Observer } from 'rxjs/internal/types';
import {HttpErrorResponse} from '@angular/common/http';
import {HttpService} from './http.service';
import {PostModel} from '../models/post.model';
import {PostsHelper} from '../helpers/posts.helper';
import {KeyValueInterface} from '../interface/key-value.interface';
import {map} from 'rxjs/internal/operators';

@Injectable({ providedIn: 'root' })

export class UserService {
  private user: UserModel;

  private userBS: BehaviorSubject<UserModel> = new BehaviorSubject<UserModel>(this.user);

  constructor(private http: HttpService) {
    // Fake user model "downloading"

  }

  public getUser(id: number): Observable<UserModel> {
    return Observable.create((observer: Observer<UserModel>): void => {
      this.http.get(`https://jsonplaceholder.typicode.com/users/${id}`).pipe(
        map((value) => value )
      ).subscribe(
        (result) => {
          observer.next(UserHelper.createUserModel(result));
          observer.complete();
        }
      );
      /*this.http.get(`https://jsonplaceholder.typicode.com/users/${id}`).subscribe(
        (result: any) => {
          observer.next(UserHelper.createUserModel(result));
          observer.complete();
        },
        (reject: HttpErrorResponse) => {
          observer.error(reject);
        }
      );*/
    });
  }
  public getUsers(url: string): Observable<Map<number, UserModel>> {
    return Observable.create((observer: Observer<Map<number, UserModel>>) => {
      this.http.get(url).subscribe(
        (result: any) => {
          console.log(result);
          const newUsers: Map<number, UserModel> = new Map<number, UserModel>();
          for (const item of result) {
            newUsers.set(item.id, UserHelper.createUserModel(item));
          }
          observer.next(newUsers);
          observer.complete();
        },
        (reject: HttpErrorResponse) => {
          observer.error(reject);
        }
      );
    });
  }

  public getUserPosts(userId: string): Observable<[UserModel, PostModel[]]> {
    return Observable.create((observer: Observer<[UserModel, PostModel[]]>) => {
      this.http.get<KeyValueInterface<any>, UserModel>(`https://jsonplaceholder.typicode.com/users/${userId}`).subscribe(
        (userModel: UserModel) => {
          const createdUserModel: UserModel = UserHelper.createUserModel(userModel);
          this.http.get<KeyValueInterface<any>, PostModel[]>(`https://jsonplaceholder.typicode.com/posts?userId=${createdUserModel.id}`).subscribe(
            (postModel: KeyValueInterface<any>[]) => {
              observer.next([createdUserModel, postModel.map<PostModel>(PostsHelper.createPostModel)]);
              observer.complete();
            }
          );
        }
      );
    });
  }
  public dispatch(user: UserModel) {
    this.next(user);
  }
  private next(user: UserModel) {
    this.user = user;
    this.userBS.next(user);
  }
}
