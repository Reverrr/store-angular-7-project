import {Injectable} from '@angular/core';
import {NewsItemModel} from '../models/newsItem.model';
import {BehaviorSubject, Observable} from 'rxjs/index';

@Injectable({providedIn: 'root'})

export class NewsService {
  public json: string = `[
    {
      "id": "1",
      "title": "TTT",
      "text": "ttttt"
    },
    {},
    {
    "id": "2",
      "title": "Qqqq",
      "text": "qqqq"
    },
    {
    "id": "3",
      "title": "WWW",
      "text": "wwww"
    },
    {
    "id": "4",
      "title": "EEE",
      "text": "eee"
    },
    {
    "id": "5",
      "title": "RRR",
      "text": "rrrr"
    }
  ]`;
  private newsItemState: NewsItemModel[];
  private newsItemBS: BehaviorSubject<NewsItemModel[]> = new BehaviorSubject<NewsItemModel[]>(this.newsItemState);

  constructor() {
    this.newsItemState = JSON.parse(this.json);
    this.newsItemBS.next(this.newsItemState);
  }
  public getSubscribe(): Observable<NewsItemModel[]> {
    return this.newsItemBS.asObservable();
  }
}
