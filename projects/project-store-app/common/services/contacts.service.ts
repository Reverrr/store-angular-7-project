import {Injectable} from '@angular/core';
import {ContactsModel} from '../models/contacts.model';
import {Observable, Observer} from 'rxjs/index';
import {ContactsHelper} from '../helpers/contacts.helper';

@Injectable({providedIn: 'root'})

export class ContactsService {
  public jsonData: string = '';
  public contacts: ContactsModel<string>[];
  constructor() {
    this.jsonData = `[
     {
      "id": "Admin",
      "value": ["Rever"],
      "isOpen": false
    },
    {
      "id": "Moderator",
      "value": ["qwerty", "asdf", "zxcvb"],
      "isOpen": false
    },
    {
      "id": "Phone",
      "value": ["+38 669 666 66 66", "+38 666 666 66 66"],
      "isOpen": false
    }
    ]`;
  }
  public getContacts (): Observable<ContactsModel<string>> {
    return Observable.create((observer: Observer<ContactsModel<string>>) => {
      JSON.parse(this.jsonData).forEach((item) => {
        observer.next(ContactsHelper.createContactsModel(item));
      });
      observer.complete();
    });
  }
}
