import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {StoreModel} from '../models/store.model';
import {BehaviorSubject, Observable, Observer} from 'rxjs/index';
import {StoreHelper} from '../helpers/store.helper';
import {KeyValueInterface} from '../interface/key-value.interface';

@Injectable({ providedIn: 'root' })

export class StoreService {
  private countStore: number = 0;
  private storeModels: StoreModel[] = [];

  private bsStore: BehaviorSubject<number> = new BehaviorSubject(this.countStore);
  private bsStoreModels: BehaviorSubject<StoreModel[]> = new BehaviorSubject<StoreModel[]>(this.storeModels);

  constructor(private http: HttpClient) {}

  public getStore(): Observable<StoreModel[]> {
    return Observable.create((observer: Observer<StoreModel[]>): void => {
      this.http.get<KeyValueInterface<any>[]>('http://localhost:3000/store1').subscribe(
        (value: KeyValueInterface<any>[]) => {
        observer.next(value.map<StoreModel>(StoreHelper.createStoreModel));
        observer.complete();
      });
    });
  }

  public getNewProduct () {
    return this.bsStoreModels.asObservable();
  }
  public getTotalProduct() {
    return this.bsStore.asObservable();
  }

  public dispatch(storeModel: StoreModel) {
    this.next(storeModel);
  }

  public changeScore(value: number) {
    this.countStore += value;
    this.bsStore.next(this.countStore);
  }

  private next(storeModel: StoreModel) {
    if (!this.storeModels.length) {
      this.storeModels.push(storeModel);
    } else if (!this.storeModels.find((x) => x.id === storeModel.id)) {
      this.storeModels.push(storeModel);
    } else if (storeModel.count === 0) {
      const index = this.storeModels.findIndex((x) => {
        return x.id === storeModel.id;
      });
      this.storeModels.splice(index, 1);
    }
    this.bsStoreModels.next(this.storeModels);
  }
}
