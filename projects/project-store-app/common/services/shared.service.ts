import {Injectable} from '@angular/core';
import {SelectorOptionModel} from '../models/selector-option.model';

@Injectable({providedIn: 'root'})

export class SharedService {
  public static cloneSelectorModel(model: SelectorOptionModel<string>[]): SelectorOptionModel<string>[] {
    const newItems: SelectorOptionModel<string>[] = [];
    for (const item of model) {
      newItems.push(new SelectorOptionModel<string>({
        label: item.label,
        value: item.value,
        checked: item.checked,
      }));
    }
    return newItems;
  }
}
