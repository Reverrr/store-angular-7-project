import { Injectable } from '@angular/core';
import {AppStateModel} from '../models/app-state.model';
import {BehaviorSubject} from 'rxjs/index';

@Injectable({ providedIn: 'root' })

export class AppStateService {
  private state: AppStateModel;

  private stateBS: BehaviorSubject<AppStateModel> = new BehaviorSubject<AppStateModel>(this.state);

  public getStateSubscription (): BehaviorSubject<AppStateModel> {
    return this.stateBS;
  }
  public dispatch(state) {
    this.state = state;
    this.stateBS.next(state);
  }
}
