import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router} from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { UserService } from '../services/user.service';
import { Observer } from 'rxjs/internal/types';

@Injectable({ providedIn: 'root' })

export class BaseResolver implements Resolve<void> {
  constructor(private userService: UserService, private router: Router) {}

  public resolve(route: ActivatedRouteSnapshot): Observable<void> {
    const userId = 2;
    return Observable.create((observer: Observer<void>): void => {
      this.userService.getUser(userId).subscribe(
        (): void => {
          if (parseInt(route.params['userId'], 10) === userId) {
            observer.next(void 0);
            observer.complete();
          }
        },
        (error): void => {
          console.log(error);
          observer.next(void 0);
          observer.complete();
        }
      );
      if (parseInt(route.params['userId'], 10) !== userId) {
        this.router.navigateByUrl('/');
        observer.complete();
      }
    });
  }
}
