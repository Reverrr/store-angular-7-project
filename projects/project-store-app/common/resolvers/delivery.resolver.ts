import {Injectable} from '@angular/core';
import {Resolve, Router} from '@angular/router';
// import {StoreService} from '../services/store.service';
import {Observable, Observer} from 'rxjs/index';

@Injectable({providedIn: 'root'})

export class DeliveryResolver implements Resolve<void> {
  // private models;
  // private sub: Subscription;
  constructor(
    // public storeService: StoreService,
    public router: Router) {}
  public resolve(): Observable<void> {
    return Observable.create((observer: Observer<void>) => {
      // this.sub = this.storeService.getNewProduct().subscribe((models) => {
      //   this.models = models;
      // });
      // /**
      //  * Redirect to /store!!!!*/
      // if (!this.models.length) {
      //   this.router.navigateByUrl('/delivery');
      // }
      // this.sub.unsubscribe();
      // this.storeService.setToggleBasketViewSubject(false);
      observer.next(void 0);
      observer.complete();
    });
  }
}
