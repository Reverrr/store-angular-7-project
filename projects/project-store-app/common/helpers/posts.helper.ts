import {PostModel} from '../models/post.model';

export class PostsHelper {
  public static createPostModel (data): PostModel {
    return new PostModel({
      id: data.id,
      userId: data.userId,
      title: data.title,
      body: data.body
    });
  }
}
