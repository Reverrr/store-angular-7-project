import { UserModel } from '../models/user.model';

export class UserHelper {
  public static createUserModel(parsed: any): UserModel {
    return new UserModel({
      id: parsed.id,
      name: parsed.name,
      username: parsed.username,
      email: parsed.email,
      address: parsed.address
    });
  }
}
