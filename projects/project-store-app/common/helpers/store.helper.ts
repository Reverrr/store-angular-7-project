import {StoreModel} from '../models/store.model';

export class StoreHelper {
  public static createStoreModel (option): StoreModel {
    return new StoreModel({
      id: option.id,
      title: option.title,
      url: option.url,
      price: option.price,
      count: option.count
    });
  }
  public static createStoreModelMap (models: StoreModel[]): Map<string, StoreModel> {
    const map = new Map<string, StoreModel>();
    models.forEach((model: StoreModel) => {
      map.set(model.id, model);
    });
    return map;
  }
  public static getFullNewStore(fullStore: Map<string, StoreModel>, selectedStore: Map<string, StoreModel>): Map<string, StoreModel>  {
    if (selectedStore.size) {
      selectedStore.forEach((model: StoreModel, key: string) => {
        fullStore.set(key, model);
      });
    }
    return fullStore;
  }

  public static totalScore(models: StoreModel[]): number {
    let total: number = 0;
    models.forEach((model: StoreModel) => total = total + parseInt(model.price, 10) * model.count);
    return total;
  }
}
