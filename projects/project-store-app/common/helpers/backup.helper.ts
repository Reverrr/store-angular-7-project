export class BackupHelper {
  public static setBackup(array) {
    const backup: any[] = [];
    array.forEach((element: any) => {
      if (!!element) {
        backup.push(element);
      }
    });
    return backup;
  }
}
