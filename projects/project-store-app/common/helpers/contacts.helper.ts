import {ContactsModel} from '../models/contacts.model';

export class ContactsHelper {
  public static createContactsModel (parsed: any): ContactsModel<string> {
    return new ContactsModel({
      id: parsed.id,
      value: parsed.value,
      isOpen: parsed.isOpen
    });
  }
}
