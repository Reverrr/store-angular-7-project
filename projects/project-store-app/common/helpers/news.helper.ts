import {NewsItemModel} from '../models/newsItem.model';

export class NewsHelper {
  public static createNewsId(options) {
    return new NewsItemModel({
      id: options.id,
      title: options.title
    });
  }
  public static createNewsModel(options) {
    return new NewsItemModel({
      id: options.id,
      title: options.title,
      text: options.text
    });
  }
}
