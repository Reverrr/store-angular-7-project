export interface StoreInterface {
  id: string;
  url: string;
  title: string;
  price: string;
  count: number;
}
