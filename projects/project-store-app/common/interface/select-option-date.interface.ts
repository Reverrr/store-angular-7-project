export interface SelectOptionDateInterface {
  key: string;
  value: string;
}
