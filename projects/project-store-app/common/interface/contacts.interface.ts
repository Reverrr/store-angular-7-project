export interface ContactsInterface<T> {
  id: string;
  value: Array<T>;
  isOpen?: boolean;
}
