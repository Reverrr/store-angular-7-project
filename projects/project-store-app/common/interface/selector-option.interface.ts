export interface SelectorOptionInterface<T> {
  label: string;
  value: T;
  checked: boolean;
}
