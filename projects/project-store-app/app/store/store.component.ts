import {Component} from '@angular/core';
import {StoreModel} from '../../common/models/store.model';
import {StoreService} from '../../common/services/store.service';
import {StoreHelper} from '../../common/helpers/store.helper';
import {Subscription} from 'rxjs/index';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: [ './store.component.scss' ]
})

export class StoreComponent {
  public storesBackup: Map<string, StoreModel> = new Map<string, StoreModel>();
  public p: number = 1;
  private subscribe: Subscription;

  constructor (public storeService: StoreService) {
    this.storeService.getStore().subscribe((modelsFull: StoreModel[]) => {
      this.storesBackup = StoreHelper.createStoreModelMap(modelsFull);
      this.subscribe = this.storeService.getNewProduct().subscribe((models: StoreModel[]) => {
        StoreHelper.getFullNewStore(this.storesBackup, StoreHelper.createStoreModelMap(models));
        if (this.subscribe) {
          this.subscribe.unsubscribe();
        }
      });
    });
  }

  public onClickBuy(id: string, e: Event) {
    e.stopPropagation();
    const temp: StoreModel | undefined = this.storesBackup.get(id);
    if (temp) {
      temp.count = 1;
      this.storeService.changeScore(1);
      this.storeService.dispatch(temp);
    }
  }

  public increase(id: string, e: Event) {
    e.stopPropagation();
    const temp: StoreModel | undefined = this.storesBackup.get(id);
    if (temp) {
      temp.count++;
      this.storeService.changeScore(1);
      this.storeService.dispatch(temp);
    }

  }

  public reduce(id: string, e: Event) {
    e.stopPropagation();
    const temp: StoreModel | undefined = this.storesBackup.get(id);
    if (temp) {
      temp.count--;
      this.storeService.changeScore(-1);
      this.storeService.dispatch(temp);
    }
  }
}
