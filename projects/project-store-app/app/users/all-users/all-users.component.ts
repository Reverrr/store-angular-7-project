import {Component} from '@angular/core';
import {UserService} from '../../../common/services/user.service';
import {UserModel} from '../../../common/models/user.model';
import {PostService} from '../../../common/services/post.service';

@Component({
  selector: 'app-all-users',
  templateUrl: 'all-users.component.html',
  styleUrls: ['all-users.component.scss']
})

export class AllUsersComponent {
  public users: UserModel[] = [] as UserModel[];
  constructor(private userService: UserService, private postService: PostService) {
    this.postService.getPostUser('3').subscribe(
      (value) => {
        console.log(value);
      }
    );
    this.postService.getPosts('https://jsonplaceholder.typicode.com/posts').subscribe();

    this.userService.getUserPosts('3').subscribe();

    this.userService.getUsers('https://jsonplaceholder.typicode.com/users').subscribe(
      (result: Map<number, UserModel>) => {
        result.forEach((value: UserModel) => {
          this.users.push(value);
        });
      },
      (reject: Error) => {
        console.log(new Error(`Err: ${reject}`));
      },
    );
  }

}
