import {Component} from '@angular/core';
import {UserService} from '../../../common/services/user.service';
import {UserModel} from '../../../common/models/user.model';
import {ActivatedRoute} from '@angular/router';
import {PostModel} from '../../../common/models/post.model';

@Component({
  selector: 'app-user',
  templateUrl: 'user.component.html',
  styleUrls: ['user.component.scss'],
})

export class UserComponent {
  public user: UserModel = {} as UserModel;
  public userPosts: PostModel[] = [];
  constructor(private userService: UserService, private userActivateRoute: ActivatedRoute) {
    this.userService.getUserPosts('3').subscribe(
      (data) => {
        this.userPosts = data[1];
      }
    );
    const userId: number = this.userActivateRoute.snapshot.params['userId'];
    this.userService.getUser(userId).subscribe(
      (result: UserModel) => {
        this.user = result;
      },
      (reject: Error) => {
        console.log(new Error(`Error: ${reject}`));
      },
    );
  }
}
