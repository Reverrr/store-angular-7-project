import {NgModule} from '@angular/core';
import {AllUsersComponent} from './all-users/all-users.component';
import {UserComponent} from './user/user.component';
import {HttpClientModule} from '@angular/common/http';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {UserService} from '../../common/services/user.service';
import {CutawayPipe} from '../../common/pipes/cutaway.pipe';
import {AsyncGetUserPipe} from '../../common/pipes/async-get-user.pipe';

@NgModule({
  declarations: [
    AllUsersComponent,
    UserComponent,
    CutawayPipe,
    AsyncGetUserPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule
  ],
  exports: [],
  providers: [UserService]
})

export class UsersModule {}
