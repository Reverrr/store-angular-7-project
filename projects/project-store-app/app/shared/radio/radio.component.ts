import {Component, EventEmitter, Input, Output} from '@angular/core';
import {BaseSharedClass} from '../../../common/base/shared-class-base';
import {SelectorOptionModel} from '../../../common/models/selector-option.model';

@Component({
  selector: 'app-radio',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.scss']
})

export class RadioComponent extends BaseSharedClass {
  public options: SelectorOptionModel<string>[];
  @Input()
  public set optionsMain(value: SelectorOptionModel<string>[]) {
    this.options = value;
  }

  @Output()
  public checkedOption: EventEmitter<[SelectorOptionModel<string>, number]> = new EventEmitter<[SelectorOptionModel<string>, number]>();
  public onChecked(item: SelectorOptionModel<string>, index): void {
    this.checkedOption.emit([item, index]);
  }
}
