import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-custom-select',
  templateUrl: './custom-select.component.html',
  styleUrls: [ './custom-select.component.scss' ]
})

export class CustomSelectComponent {
  public selectArr: any[] = [];
  public selectedKey;
  public dropMenuToggle: boolean;
  @Input()
  public set setOptions(options) {
    this.selectedKey = options[0];
    options.forEach((obj) => {
      obj.selected = false;
      this.selectArr.push(obj);
    });
    this.selectArr[0].selected = true;
  }
  constructor () {}
  public onClickDropMenu() {
    this.dropMenuToggle = !this.dropMenuToggle;
  }
  public selectedOption(index) {
    this.selectedKey = this.selectArr[index];
    this.dropMenuToggle = false;
  }
}
