import {Component, EventEmitter, Input, Output} from '@angular/core';
import {BaseSharedClass} from '../../../common/base/shared-class-base';
import {SelectorOptionModel} from '../../../common/models/selector-option.model';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})

export class CheckboxComponent extends BaseSharedClass {
  public options: SelectorOptionModel<string>;

  @Input()
  public set optionsMain(value) {
    this.options = value;
    console.log(this.options);
  }
  @Output()
  public checkedOption: EventEmitter<SelectorOptionModel<string>> = new EventEmitter<SelectorOptionModel<string>>();
  public onChecked(item: SelectorOptionModel<string>) {
    this.checkedOption.emit(item);
  }
}
