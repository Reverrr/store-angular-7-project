import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { SelectComponent } from './select/select.component';
import { CheckboxComponent } from './checkbox/checkbox.component';
import {RadioComponent} from './radio/radio.component';
import {CustomSelectComponent} from './custom-select/custom-select.component';

@NgModule({
  declarations: [
    RadioComponent,
    SelectComponent,
    CheckboxComponent,
    CustomSelectComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    RadioComponent,
    SelectComponent,
    CheckboxComponent,
    CustomSelectComponent
  ]
})

export class SharedModule {}
