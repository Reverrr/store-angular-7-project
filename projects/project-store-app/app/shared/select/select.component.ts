import {Component, EventEmitter, Input, Output} from '@angular/core';
import {BaseSharedClass} from '../../../common/base/shared-class-base';
import {SelectorOptionModel} from '../../../common/models/selector-option.model';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})

export class SelectComponent extends BaseSharedClass {
  public options: SelectorOptionModel<string>[];
  public prevOption: number = 0;
  @Input()
  public set optionsMain(value: SelectorOptionModel<string>[]) {
    this.options = value;
  }
  @Output()
  public checkedOption: EventEmitter<number[]> = new EventEmitter<number[]>();
  public checkedChange(event): void {
    if (this.prevOption !== event.target.selectedIndex) {
      this.checkedOption.emit([this.prevOption, event.target.selectedIndex]);
      this.prevOption = event.target.selectedIndex;
    }
  }
}
