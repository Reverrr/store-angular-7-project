import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {RegistrationValidator} from '../../common/validations/registration.validator';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})

export class RegistrationComponent {
    public registrationForm: FormGroup = new FormGroup({
      login: new FormControl('', [
        Validators.required
      ]),
      email: new FormControl('', [
        Validators.required,
        RegistrationValidator.emailValidator
      ]),
      password: new FormControl('', [
        Validators.required
      ]),
      password2: new FormControl('', [
        Validators.required
      ])
    });

  public onCheck() {
      this.registrationForm.reset();
  }
}
