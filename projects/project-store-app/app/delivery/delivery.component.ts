import {Component} from '@angular/core';
import {StoreModel} from '../../common/models/store.model';
import {StoreService} from '../../common/services/store.service';
import {Subscription} from 'rxjs/index';
import {SelectedProductComponent} from '../selected-product/selected-product.component';
import {LayoutService} from '../../common/services/layout.service';
import {StoreHelper} from '../../common/helpers/store.helper';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {SelectOptionDateModel} from '../../common/models/select-option-date.model';

@Component({
  selector: 'app-delivery',
  templateUrl: './delivery.component.html',
  styleUrls: [ './delivery.component.scss', '../app.component.scss' ],
})

export class DeliveryComponent extends SelectedProductComponent {
  public storeModel: StoreModel[] = [];
  public totalPrice: number;
  public isCustomDate: boolean = false;
  public selectOptionsDate: SelectOptionDateModel[] =
    new Array(new SelectOptionDateModel({key: 'now', value: 'Now!'}),
              new SelectOptionDateModel({key: 'custom', value: 'Select date'}));
  public deliveryFormGroup: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required,
      Validators.maxLength(15)]),
    phone: new FormControl('', [Validators.required,
      Validators.maxLength(9),
      Validators.pattern(/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/)]),
    location: new FormControl('', [Validators.required]),
    date: new FormControl(``, [Validators.required]),
    time: new FormControl(``, [Validators.required])
  });
  // private backupSelectOptionsDate: any[] = [];
  private subGetNewProduct: Subscription;
  constructor(public storeService: StoreService,
              public layOut: LayoutService,
              public router: Router) {
    super(storeService, layOut);

    // this.backupSelectOptionsDate = BackupHelper.setBackup(this.selectOptionsDate);
    this.subGetNewProduct = this.storeService.getNewProduct().subscribe((model: StoreModel[]) => {
      this.storeModel = model;
      if (this.subGetNewProduct && model) {
        this.subGetNewProduct.unsubscribe();
      }
    });
  }
  public removeItem(index: number, event) {
    super.removeSelectedProduct(index, event);
    this.totalPrice = StoreHelper.totalScore(this.storeModel);
    if (!this.storeModel.length) {
      this.router.navigateByUrl('/store');
    }
  }
  public increase(index: number, event) {
    super.increase(index, event);
    this.totalPrice = StoreHelper.totalScore(this.storeModel);
  }
  public reduce(index: number, event) {
    super.reduce(index, event);
    this.totalPrice = StoreHelper.totalScore(this.storeModel);
    if (!this.storeModel.length) {
      this.router.navigateByUrl('/store');
    }
  }
  public submit() {
    const nowDate = new Date();
    console.log(this.deliveryFormGroup.value);
    if (this.deliveryFormGroup.value.date === 'now') {
      this.deliveryFormGroup.value.date =
        `${nowDate.getFullYear()}
        -${nowDate.getMonth()}
        -${nowDate.getDate()}
        |${nowDate.getHours()}
        :${nowDate.getMinutes()}`;
    }
    console.log(this.deliveryFormGroup.value);
    return this.deliveryFormGroup.value;
  }
  public setCustomDate(event) {
    switch (event.target.value) {
      case 'custom': {
        return this.isCustomDate = true;
      }
      case 'now': {
        return this.isCustomDate = false;
      }
      default: {
        return this.isCustomDate = true;
      }
    }
  }
}
