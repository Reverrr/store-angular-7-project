import {Component, EventEmitter, Output} from '@angular/core';
import {StoreService} from '../../common/services/store.service';
import {StoreModel} from '../../common/models/store.model';
import {Subscription} from 'rxjs/index';
import {StoreHelper} from '../../common/helpers/store.helper';
import {LayoutService} from '../../common/services/layout.service';


@Component({
  selector: 'app-selected-product',
  templateUrl: './selected-product.component.html',
  styleUrls: [ './selected-product.component.scss', '../app.component.scss' ]
})

export class SelectedProductComponent {
  public selectedProduct: StoreModel[];
  public currentScore: number;
  public getNewProductSubscription: Subscription;
  public mouseEventSubscription: Subscription;
  public totalPrice: number;
  public isOpenStoreProducts: boolean = true;

  @Output()
  public onclick: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output()
  public redirect: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(public storeService: StoreService, public layOut: LayoutService) {
    this.getNewProductSubscribe();
    this.mouseEventSubscription = this.layOut.mouseEvent.subscribe((event: EventTarget) => {
      if (!event['target'].closest('#open-store-products')) {
        this.isOpenStoreProducts = false;
        this.getNewProductSubscription.unsubscribe();
        this.mouseEventSubscription.unsubscribe();
        this.onclick.emit(false);
      }
    });
    this.storeService.getTotalProduct().subscribe(
      (value: number) => {
        this.currentScore = value;
      });
  }

  public getNewProductSubscribe() {
    this.getNewProductSubscription = this.storeService.getNewProduct().subscribe(
      (value: StoreModel[]) => {
        this.totalPrice = StoreHelper.totalScore(value);
        this.selectedProduct = value;
      });
  }
  public onClickShopIcon() {
    this.getNewProductSubscription.unsubscribe();
    this.mouseEventSubscription.unsubscribe();
    this.isOpenStoreProducts = false;
    this.onclick.emit(false);
  }
  public redirectToDelivery() {
    this.onClickShopIcon();
    this.redirect.emit(false);

  }
  public increase(index: number, event) {
    this.generalCountingLogic(index, event, 1);
  }
  public reduce(index: number, event) {
    this.generalCountingLogic(index, event, -1);
  }
  public removeSelectedProduct(index: number, event) {
    this.generalCountingLogic(index, event, -this.selectedProduct[index].count);
    this.totalPrice = StoreHelper.totalScore(this.selectedProduct);
  }
  private generalCountingLogic(index: number, event, changeScore: number) {
    event.stopPropagation();
    this.currentScore -= this.selectedProduct[index].count;
    this.selectedProduct[index].count = this.selectedProduct[index].count + changeScore;
    this.storeService.changeScore(changeScore);
    if (this.currentScore === 0) {
      this.onclick.emit(false);
    }
    this.storeService.dispatch(this.selectedProduct[index]);
  }
}
