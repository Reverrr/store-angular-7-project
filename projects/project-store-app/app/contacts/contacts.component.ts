import {Component} from '@angular/core';
import {ContactsService} from '../../common/services/contacts.service';
import {ContactsModel} from '../../common/models/contacts.model';
import {isBoolean} from 'util';
import {LocalstorageService} from '../../common/services/localstorage.service';

@Component({
  selector: 'app-contacts',
  templateUrl: 'contacts.component.html',
  styleUrls: ['contacts.component.scss']
})

export class ContactsComponent {
  public contacts: ContactsModel<string>[] = [];
  constructor(private contactsService: ContactsService, public local: LocalstorageService) {
    this.contactsService.getContacts().subscribe(
      (data: ContactsModel<string>) => {
        this.contacts.push(data);
      }
    );
    console.log(local.get('contactsTabs'));
  }

  public contactsClick(index: number): void {
    this.contacts[index].isOpen = !this.contacts[index].isOpen;
  }

  public getOpenedTab (): boolean[] {
    const isOpenedTabs: boolean[] = [];
    this.contacts.forEach((item: ContactsModel<string>): void => {
      console.log(typeof undefined);
      if (typeof item.isOpen === 'boolean' && isBoolean(item.isOpen)) {
        isOpenedTabs.push(item.isOpen);
      }
    });
    this.local.set('contactsTabs', `[${isOpenedTabs}]`);
    return isOpenedTabs;
  }
}
