import {Component} from '@angular/core';
import {LayoutService} from '../../common/services/layout.service';
import {NewsService} from '../../common/services/news.service';
import {NewsItemModel} from '../../common/models/newsItem.model';

@Component({
  selector: 'app-news',
  templateUrl: 'news.component.html',
  styleUrls: ['news.component.scss']
})

export class NewsComponent {
  public news: NewsItemModel[];
  public dropMenu: boolean = false;
  public selectedId: NewsItemModel | undefined;

  constructor(public layout: LayoutService, public newsService: NewsService) {
    this.layout.mouseEvent.subscribe(() => {
      this.dropMenu = false;
    });
    this.newsService.getSubscribe().subscribe((value) => {
      console.log(value);
    });
  }
  public dropMenuToggle(event: MouseEvent): void {
    event.stopPropagation();
    this.dropMenu = !this.dropMenu;
  }
  public selectedDropMenu(event: MouseEvent): void {
    event.stopPropagation();
  }
}
