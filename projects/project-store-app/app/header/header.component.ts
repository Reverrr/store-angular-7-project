import {Component} from '@angular/core';
import {StoreService} from '../../common/services/store.service';
import {NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: [ './header.component.scss' ]
})

export class HeaderComponent {

  public currentScore: number;
  public onClickImageBool: boolean = false;
  public toggleBasketView: boolean = true;

  constructor (public storeService: StoreService, public router: Router) {
    this.router.events.subscribe((event): void => {
      if (event instanceof NavigationEnd) {
        if (event.url === '/delivery') {
          this.toggleBasketView = false;
        } else {
          this.toggleBasketView = true;
        }
      }
    });
    this.storeService.getTotalProduct().subscribe((value: number) => {
      this.currentScore = value;
    });
  }

  public onClickImage(status: boolean) {
    this.onClickImageBool = status;
  }
  public toggleOpenTabShop(event: Event): void {
    event.stopPropagation();
    if (this.currentScore) {
      this.onClickImageBool = !this.onClickImageBool;
    }
  }
  public getStatusToggleBasketView(status: boolean) {
    this.toggleBasketView = status;
  }
}
