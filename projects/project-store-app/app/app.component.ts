import { Component } from '@angular/core';
import {AppStateService} from '../common/services/app-state.service';
import {AppStateModel} from '../common/models/app-state.model';
import {ActivationStart, Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})

export class AppComponent {
  public stateObject: AppStateModel = {} as AppStateModel;
  public currentUrl: string;
  public currentComponent: string;
  constructor(
    private appState: AppStateService,
    private router: Router,
  ) {
    this.router.events.subscribe(
      (event): void => {
      if (event instanceof ActivationStart) {
        if (!!event.snapshot.component && !!event.snapshot.routeConfig && event.snapshot.routeConfig.path !== undefined) {
          this.currentComponent = event.snapshot.component['name'];
          this.currentUrl = event.snapshot.routeConfig.path;
          this.stateObject = new AppStateModel({
            url: this.currentUrl,
            component: this.currentComponent
          });
          this.appState.dispatch(this.stateObject);
        }
      }
    });
  }
}

