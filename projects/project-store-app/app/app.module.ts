import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {SharedModule} from './shared/shared.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BaseComponent} from './base/base.component';
import {MainPageComponent} from './main-page/main-page.component';
import {RouterModule} from '@angular/router';
import {ROUTES} from '../common/const/routes.const';
import {ContactsComponent} from './contacts/contacts.component';
import {AboutUsComponent} from './about-us/about-us.component';
import {NewsComponent} from './news/news.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {UsersModule} from './users/users.module';
import {RegistrationComponent} from './registration/registration.component';
import {LoginComponent} from './login/login.component';
import {Interceptor} from '../common/services/http-interceptor.service';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {ContactsGuard} from '../common/guards/contacts.guard';
import {BaseResolver} from '../common/resolvers/base.resolver';
import {HeaderComponent} from './header/header.component';
import {StoreComponent} from './store/store.component';
import {SelectedProductComponent} from './selected-product/selected-product.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {DeliveryComponent} from './delivery/delivery.component';
import {TransformMapToArrayPipe} from '../common/pipes/transform-map-to-array.pipe';

@NgModule({
  declarations: [
    AppComponent,
    BaseComponent,
    MainPageComponent,
    NewsComponent,
    ContactsComponent,
    AboutUsComponent,
    NotFoundComponent,
    RegistrationComponent,
    LoginComponent,
    HeaderComponent,
    StoreComponent,
    SelectedProductComponent,
    DeliveryComponent,
    TransformMapToArrayPipe,
  ],
  imports: [
    BrowserModule,
    SharedModule,
    FormsModule,
    NgbModule,
    UsersModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    RouterModule.forRoot(ROUTES),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true
    },
    ContactsGuard,
    BaseResolver
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
